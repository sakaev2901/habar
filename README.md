# HABAR



## Инструкция по удалению аккаунта и данных связанных с ним в приложении HABAR 

Для удаления аккаунта и данных связанных с ним следуйте следующей инструкции: 

- Зайдите в свой профиль 
- Нажмие на шестеренку чтобы зайти в настройки аккаунта (левый верхний угол)
- В самом низу нажмиту на кнопку "Удалить мой аккаунт"
- Подтвердите удаление

Так вы удалите все данные связанные с вами. Останутся только информации о ваших записях к врачам

